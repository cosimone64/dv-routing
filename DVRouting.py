# Author: Cosimo Agati


class DVRouting:
    def __init__(self, graph):
        """Initializes list of dictionaries"""
        for node in graph:
            self.check_weights(node)
        self.check_self_links(graph)
        self.graph = graph
        self.num_nodes = len(self.graph)
        self.dvlist = self.init_table()
        self.routing_table = self.init_table()
        self.to_modify = [True for i in range(len(self.graph))]
        self.check_new_paths()

    def check_weights(self, node):
        """Checks if link weights in the network are valid"""
        for link in node.keys():
            if node[link] < 0:
                raise Exception('Negative weight in parameters')

    def check_self_links(self, graph):
        """Checks if self link values are correct"""
        for i in range(len(graph)):
            try:
                cost = graph[i][i]
            except KeyError:
                continue
            if cost != 0:
                raise Exception('Invalid self link cost')

    def init_table(self):
        """Initializes dvlist with default values"""
        dvlist = []
        nodes = range(len(self.graph))
        for i in nodes:
            dvlist = dvlist + [[]]
            for j in nodes:
                dvlist[i] = dvlist[i] + self.get_dvlist_elem(i, j)
        return dvlist

    def get_dvlist_elem(self, i, j):
        """Returns the initial element for the dvlist in [i][j]"""
        if i == j:
            return [0]
        else:
            try:
                return [self.graph[i][j]]
            except KeyError:
                return [None]

    def weight(self, x, y):
        """Returns weight of the link between nodes x and y"""
        if x == y:
            return 0
        try:
            return self.graph[x][y]
        except (KeyError, IndexError):
            return

    def set(self, x, y, w):
        """Sets weight of the link between x and y to the value w"""
        if (x == y and w != 0) or (w < 0 and w is not None):
            raise Exception('Invalid weight')
        try:
            self.graph[y]
        except IndexError:
            raise Exception('Specified node does not exist')
        if w is not None:
            del self.graph[x][y], self.graph[y][x]
        else:
            self.graph[x][y] = self.graph[y][x] = w

    def add(self):
        """Adds a new node in the network. Returns its index"""
        new_index = len(self.graph)
        self.graph = self.graph + [{}]
        self.dvlist = self.add_table_elem(self.dvlist)
        self.routing_table = self.add_table_elem(self.routing_table)
        self.to_modify = self.to_modify + [True]
        return new_index

    def add_table_elem(self, table):
        table = [row + [None] for row in table]
        table = table + [[None for i in range(len(self.graph))]]
        table[-1][-1] = 0
        return table

    def remove(self, x):
        """Removes all links connected to the node indexed by x"""
        try:
            if self.graph[x] != {}:
                self.graph[x].clear()
            else:
                return
        except IndexError:
            return
        for node in filter(lambda a: a != x, self.graph):
            try:
                del node[x]
            except KeyError:
                continue

    def setdv(self, dvlist):
        """Sets the current dvlist with a new one"""
        if not self.check_dvlist_size(dvlist):
            raise Exception('Incorrect dimensions in DVList')
        for i in range(len(dvlist)):
            for j in range(len(dvlist)):
                self.check_new_dv_elem(i, j, dvlist)
        self.dvlist = dvlist
        self.check_new_paths()

    def check_dvlist_size(self, dvlist):
        """Checks if the new dvlist has the correct dimensions"""
        if len(dvlist) != len(self.graph):
            return False
        for dvector in dvlist:
            if len(dvector) != len(dvlist):
                return False
        return True

    def check_new_dv_elem(self, i, j, dvlist):
        """Checks corectness of the new element in position [i][j]"""
        if (i != j and dvlist[i][j] == 0) or (i == j and dvlist[i][j] != 0):
            raise Exception('Invalid weights')

    def getdv(self, x):
        """Returns the distance vector of the node indexed by x"""
        if not 0 <= x < len(self.graph):
            return
        return self.dvlist[x]

    def step(self):
        """Executes a step of the algorithm."""
        self.dvlist = self.check_new_paths()
        return self.isstable()

    def check_new_paths(self):
        """Checks the minimum path with the current dvs"""
        nodes = range(len(self.graph))
        tmp_dvlist = [row[:] for row in self.dvlist]
        for node in nodes:
            self.update_tables(node, tmp_dvlist[node])
        self.to_modify = map(lambda a: tmp_dvlist[a] != self.dvlist[a], nodes)
        return tmp_dvlist

    def update_tables(self, x, tmp_dv):
        """Updates distance vector of the node x"""
        for y in range(len(self.graph)):
            weight_list = self.get_new_weight(x, y)
            tmp_dv[y] = min(weight_list)
            self.routing_table[x][y] = weight_list.index(tmp_dv[y])

    def get_new_weight(self, x, y):
        """Returns list of possible new weights for D_x[y]"""
        if x == y:
            return [0]
        node = self.graph[x]
        weight_list = [float('inf') for i in range(len(self.graph))]
        for v in filter(lambda a: a != x, node.keys()):
            if self.dvlist[v][y] is not None:
                weight_list[v] = node[v] + self.dvlist[v][y]
        if weight_list == [float('inf') for i in range(len(self.graph))]:
            return [None]
        return weight_list

    def isstable(self):
        """Returns True if the network is stable, False otherwise"""
        return self.to_modify == [False for i in range(len(self.graph))]

    def compute(self):
        """Executes steps until stable. Returns the number of steps made"""
        iterations = 0
        while not self.step():
            iterations = iterations + 1
        return iterations

    def route(self, x, y):
        """Returns the next node to route and the total path cost"""
        try:
            self.graph[x]
            self.graph[y]
        except IndexError:
            return
        if x == y:
            return x, 0
        if None in (self.dvlist[x][y], self.routing_table[x][y]):
            return
        return self.routing_table[x][y], self.dvlist[x][y]
